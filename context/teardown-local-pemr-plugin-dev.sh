#!/usr/bin/env bash

docker service rm brain webui websock
docker network rm mcp
docker network prune
docker swarm leave -f

echo -e "OPTIONAL: run\n\tdocker image prune"
