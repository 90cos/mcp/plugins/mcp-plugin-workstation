#!/usr/bin/env bash

docker service scale webui=0
docker service scale websock=0
docker service scale brain=0
sleep 2
docker service scale brain=1
docker service scale webui=1
docker service scale websock=1
