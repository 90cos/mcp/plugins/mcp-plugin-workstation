PEMR Plugin Setup
================

Run initial setup to download the PEMR plugin dependencies


```shell
./initial-setup.sh
```

run the setup script to build the docker swarm environment and launch pemr-core containers

```shell
./setup-local-pemr-plugin-dev.sh
```

You should have 3 containers running

```shell
$ docker service ls
ID             NAME      MODE         REPLICAS   IMAGE                                                   PORTS
svcqmaczit0i   brain     replicated   1/1        registry.gitlab.com/90cos/mcp/database-brain:latest     *:28015->28015/tcp
xhevz1r37pzy   websock   replicated   1/1        registry.gitlab.com/90cos/mcp/websocket-server:latest   *:3000->3000/tcp
kgpq9yv6tb2i   webui     replicated   1/1        registry.gitlab.com/90cos/mcp/frontend-ui:latest        *:8080->8080/tcp

```

If you want to clean the brain/database to wipe old data scale to 0 and back to 1

```shell
docker service scale brain=0; docker service scale brain=1
```

