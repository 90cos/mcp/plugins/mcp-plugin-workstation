#!/usr/bin/env bash
PLUGIN_PATH=$1
PLUGIN_ROOT=$(dirname $PLUGIN_PATH)
PLUGIN_FLDR=$(basename $PLUGIN_PATH)

function drop_command_template {
cat << COMMAND_EOF > ${PLUGIN_PATH}/__${PLUGIN_FLDR}_extra/commands.py
"""
MAPL formatted command advertisement list
"""

from .tooltips import COMMAND1_TT, COMMAND2_TT

command_templates = [
    {
        "CommandName": "command1",
        "Tooltip": COMMAND1_TT,
        "Output": False,
        "Inputs": [],
        "OptionalInputs": []
    },
    {
        "CommandName": "command2",
        "Tooltip": COMMAND2_TT,
        "Output": True,
        "Inputs": [
            {
                "Name": "Process ID",
                "Type": "textbox",
                "Tooltip": "Number of the process",
                "Value": ""
            },
            {
                "Name": "Process Name",
                "Type": "textbox",
                "Tooltip": "string representing the process name ",
                "Value": "you can put a default value"
            },
        ],
        "OptionalInputs": []
    },
]
COMMAND_EOF
}


function drop_run_template {
cat << RUN_EOF > ${PLUGIN_PATH}/__${PLUGIN_FLDR}_extra/run.py
"""
generally want the ${PLUGIN_FLDR}.py _start function

to call into this module for a "run loop"

you may call the run loop whatever you want (see other plugins)

Within the run loop you want to
1. Listen for new clients (and thread/fork off connections to a worker)
2. Check for new jobs for every connected client
3. Deliver Jobs to the client
4. Check for responses
5. Upload responses or new files to the brain
6. (Optionally) sleep for a little bit
"""
from time import sleep


def run_loop():
    """
    likely want to thread off some workers at this point
    see harness (Flask Based) or others.
    """
    # start real work
    while True:
        sleep(1)
    # may not wish to ever return from here unless raising an exception
RUN_EOF
}

function drop_tooltip_template {
cat << TT_EOF > ${PLUGIN_PATH}/__${PLUGIN_FLDR}_extra/tooltips.py
"""
These tooltips are intended to fit within the
CommandBuilder area of a deployed PEMR
"""
COMMAND1_TT = """
COMMAND 1

This is a command that should be replaced

Arguments:
None

Returns:
None

"""

COMMAND2_TT = """
COMMAND 2

This is a command that has
inputs and outputs
and should be replaced

Arguments:
1. Process ID (integer)
1. Process Name (string)

Returns:
String of DATA to the console

"""
TT_EOF
}

function drop_entry_template {
cat << ENTRY_EOF > ${PLUGIN_PATH}/${PLUGIN_FLDR}.py
"""
This module imports the controls needed to interact with
${PLUGIN_FLDR}
"""
from os import environ as _environ

# first assume we are in a deployed PEMR build
# pylint: disable=import-error
# doing path shenanigans to add CWD to python path so this module works
import os.path
import sys
# should have already run initial setup and added
from plugalone.vendor.controller_plugin import ControllerPlugin
_path = os.path.abspath(os.path.join(os.path.abspath(__file__), "../"))
sys.path.append(_path)
# pylint: disable=wrong-import-position
from __${PLUGIN_FLDR}_extra.run import run_loop
from __${PLUGIN_FLDR}_extra.commands import command_templates
# pylint: enable=wrong-import-position
# pylint: enable=import-error
PLUGIN_NAME = "${PLUGIN_FLDR}"


class ${PLUGIN_FLDR}(ControllerPlugin):
    """
    Extends the PEMR base class to implement core features
    """

    functionality = command_templates  # base class requires this as a class var

    def __init__(self):
        self.name = PLUGIN_NAME
        self.debug = _environ.get("STAGE") == "DEV"
        self.proto = "TCP"  # may need to change this
        self.running = True
        super().__init__(self.name, self.functionality)

    def _start(self, *args):
        """
        this is the function you must override, understand this is backwards :-(
        call the base plugin's start function which calls this _start
        """
        run_loop()  # by convention, you want this to block forever until an exception

    def _stop(self):
        """
        anything you want to do before shutdown
        """
        return


if __name__.endswith("__main__"):
    _environ["STAGE"] = _environ.get("STAGE", "DEV")
    if _environ["STAGE"] == "DEV":
        _environ["PORT"] = str(_environ.get("PORT", 1234))  # required even if you don't use it
        _environ["PLUGIN_NAME"] = PLUGIN_NAME
        _environ["PLUGIN_ID"] = PLUGIN_NAME
        _environ["SERVICE_NAME"] = f"{PLUGIN_NAME}-DEBUGGING"
        _environ["SERVICE_ID"] = f"{PLUGIN_NAME}-DEBUGGING"
        _environ["PLUGIN"] = PLUGIN_NAME  # resolve backward compatibility issues
        _environ["LOGLEVEL"] = "DEBUG"
        _environ["EXT_INT_PORT"] = "80/tcp"
        _environ["RETHINK_HOST"] = str(_environ.get("RETHINK_HOST", "0.0.0.0"))  # your IP
        _environ["AUTO_TARGET"] = str(_environ.get("AUTO_TARGET", "1"))
        # by convention, contacts are only added as targets manually
        # if started with AUTO_TARGET, the plugin may add new contacts automatically as targets
        # starting the debug process
        # pylint: disable=ungrouped-imports
        from plugalone.utils import make_target, remove_target, destroy_plugin, make_plugin
        # pylint: enable=ungrouped-imports
        # correct way to remove a plugin's table is broken in 1.88
        # from brain.static import PLUGINDB
        # from brain.assist.database import nuke_table
        # nuke_table(PLUGINDB, PLUGIN_NAME)  # broke - brain issue resolved in ver 2

        # old way to nuke the table
        from brain.static import RPX
        from brain import connect, r
        conn = connect()
        try:
            # trying to nuke any old versions in the local DB
            RPX.table(PLUGIN_NAME).delete().run(conn)
        except r.errors.ReqlOpFailedError:
            pass
        # 'redeploy' the plugin and dev target
        destroy_plugin()
        make_plugin()
        remove_target()
        make_target()
    plgn = ${PLUGIN_FLDR}()
    plgn.start()
ENTRY_EOF
}


function drop_configuration_template {
cat << CONFIG_EOF > ${PLUGIN_PATH}/${PLUGIN_FLDR}.conf
{
  "os_choice": 2,
  "extra_sel": 1,
  "dirs": ["__${PLUGIN_FLDR}_extra"]
}
CONFIG_EOF
}

function drop_requirement_template {
cat << PIPREQ_EOF > ${PLUGIN_PATH}/__${PLUGIN_FLDR}_extra/requirements_${PLUGIN_FLDR}.txt
plugalone
PIPREQ_EOF
}


function drop_dockerfile {
cat << DOCKER_EOF > ${PLUGIN_PATH}/Dockerfile
# This can base on any image, does not need to be alpine
FROM alpine:3.18
LABEL PLUGIN_NAME=${PLUGIN_FLDR}
DOCKER_EOF
}

echo "Attempting to create plugin $PLUGIN_FLDR located at $PLUGIN_ROOT"
if [ -d "$PLUGIN_PATH" ]; then
  echo "$PLUGIN_FLDR exists - beginning to create basic module"
  if [ -f "$PLUGIN_PATH/__init__.py" ]; then
    echo "there is already an init file in $PLUGIN_PATH, exiting now"
  else
    touch "$PLUGIN_PATH/Dockerfile"
    touch "$PLUGIN_PATH/__init__.py"
    touch "$PLUGIN_PATH/${PLUGIN_FLDR}.py"
    mkdir -p "${PLUGIN_PATH}/__${PLUGIN_FLDR}_extra"
    touch "${PLUGIN_PATH}/__${PLUGIN_FLDR}_extra/requirements_${PLUGIN_FLDR}.txt"
    touch "${PLUGIN_PATH}/__${PLUGIN_FLDR}_extra/__init__.py"
    touch "${PLUGIN_PATH}/__${PLUGIN_FLDR}_extra/commands.py"
    touch "${PLUGIN_PATH}/__${PLUGIN_FLDR}_extra/run.py"
    touch "${PLUGIN_PATH}/__${PLUGIN_FLDR}_extra/tooltips.py"
    touch "${PLUGIN_PATH}/__${PLUGIN_FLDR}_extra/static.py"
    drop_tooltip_template;
    drop_entry_template;
    drop_command_template;
    drop_configuration_template;
    drop_run_template;
    drop_dockerfile;
    drop_requirement_template;
  fi
else
  echo $PLUGIN_PATH must be a folder
fi

