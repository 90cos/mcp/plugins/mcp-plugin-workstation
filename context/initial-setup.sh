#!/usr/bin/env bash

# This script should be run as the developer user at runtime to pull the newest wheel files.

pip install plugalone --index-url https://gitlab.com/api/v4/projects/37969266/packages/pypi/simple \
                      --extra-index-url https://gitlab.com/api/v4/projects/25582434/packages/pypi/simple
