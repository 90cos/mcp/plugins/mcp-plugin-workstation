#!/usr/bin/env bash


docker pull registry.gitlab.com/90cos/mcp/frontend-ui:latest
docker pull registry.gitlab.com/90cos/mcp/websocket-server:latest
docker pull registry.gitlab.com/90cos/mcp/database-brain:latest

INTERFACES=( $(ls /sys/class/net | grep -e "^e") )
docker swarm init --advertise-addr ${INTERFACES[0]}

docker network prune -f
docker network create --attachable -d overlay mcp

# switching to services to make up/down easier.

docker service create -p 28015:28015 --name brain --network mcp registry.gitlab.com/90cos/mcp/database-brain:latest
#docker run -d --rm --name brain --network mcp -p 28015:28015 registry.gitlab.com/90cos/mcp/database-brain:latest
echo "sleeping for brain to start"
sleep 5
docker service create --name webui --network mcp -p 8080:8080 -e RETHINK_HOST=brain -e STAGE=PROD registry.gitlab.com/90cos/mcp/frontend-ui:latest
#docker run -d --rm --name webui --network mcp -p 8080:8080 -e RETHINK_HOST=brain -e STAGE=PROD registry.gitlab.com/90cos/mcp/frontend-ui:latest
echo "sleeping for UI to start"
sleep 5
docker service create --name websock --network mcp -p 3000:3000 -e RETHINK_HOST=brain -e STAGE=PROD registry.gitlab.com/90cos/mcp/websocket-server:latest
#docker run -d --rm --name websock --network mcp -p 3000:3000 -e RETHINK_HOST=brain -e STAGE=PROD registry.gitlab.com/90cos/mcp/websocket-server:latest
echo "sleeping for websocket to start"
sleep 5
docker ps
echo -e "\n\nVisit:\n\thttp://localhost:8080\n\n"

