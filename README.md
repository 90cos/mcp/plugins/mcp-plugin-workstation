# MCP-plugin-workstation

tools and scripts used to generate a plugin development system, including libraries, IDE, and workspace for standalone plugin development.

## Prerequisites
You should be running on modern windows (10+/server2019+) or modern Linux (ubuntu20.04+ or/etc) and have the following installed on the host 

1. modern virtualbox >v6 installed on your system (https://www.virtualbox.org/wiki/Downloads)
2. Vagrant (https://www.vagrantup.com/downloads)

## Building the VM
by default, the credentials follow those set upstream - username: `vagrant` password: `vagrant`

if the host is running windows
```shell
del -Recurse .vagrant; vagrant up
```

if the host is running linux
```shell
rm -rf .vagrant && vagrant up
```

in either case, to destroy the VM when complete, you may use
```shell
vagrant destroy -f 
```

## Using the Virtual Machine

See `dev-setup.webm` within `using_the_vm` in this repository's package manager

https://gitlab.com/90cos/mcp/plugins/mcp-plugin-workstation/-/packages

In general you should perform the following (shown in the setup video)

1. run `initial-setup.sh` to clone the Plugin API (reboot)
2. on the first boot, run `setup-local-perm-plugin-dev.sh` which will start the services used for the Database, WebUI, and websocket server.  These will be restarted at every reboot.
3. Create a repo in the 90cos/mcp/plugins folder (video uses `Example`).  
   1. By convention, capitalization matters and the first letter of the plugin is capital.  Gitlab defaults to all lowercase.
4. run `bootstrap-plugin-dev-folder.sh` on the newly cloned folder to create the initial content.
   1. By convention, A plugin within a folder "Example" is named "Example"
   2. `Example.py` will be created that is the main entrypoint and extension of the base class
   3. By convention, all extra modules go in a double-underscore prefixed folder `__Example_extras`
      1. within that folder `commands.py` is pre-populated with exemplar command templates that should be changed
      2. within that folder `tooltips.py` is pre-populated and used within the commands structure
      3. within that folder `run.py` contains a `run_loop` function that is generally the *real* entrypoint to your new plugin.
   5. Configure your IDE to line-by-line debug using breakpoints, including into the `brain` library installed during `setup-localpemr-plugin-dev.sh`

The UI will be available at `http://localhost:8080`

if you see an nginx error, wait a moment and reload the page. 

You can wipe/reload the UI/brain services without rebooting using `teardown-local-plugin-pemr-dev.sh` and re-running `setup-local-pemr-plugin-dev.sh`
